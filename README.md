# goautocert - 定时任务管理+ssl证书自动续期与部署系统
[![license](https://img.shields.io/github/license/mashape/apistatus.svg?maxAge=2592000)](https://github.com/ouqiang/gocron/blob/master/LICENSE)

非常感谢[定时任务管理系统](https://github.com/ouqiang/gocron)

# 项目简介

## 定时任务管理
使用Go语言开发的轻量级定时任务集中调度和管理系统, 用于替代Linux-crontab [查看文档](https://github.com/ouqiang/gocron/wiki)
  
## ssl证书自动续期与部署
支持自动从Let's Encrypt 申请、续期、部署在阿里云slb上。

## 功能特性
* Web界面管理定时任务
* crontab时间表达式, 精确到秒
* 任务执行失败可重试
* 任务执行超时, 强制结束
* 任务依赖配置, A任务完成后再执行B任务
* 账户权限控制
* ssl证书申请：根据提供的域名信息、阿里云AccessKey、邮件信息自动申请ssl证书；
* ssl证书注销：根据已有的账号信息注销指定证书；
* 任务类型
    * shell任务
    > 在任务节点上执行shell命令, 支持任务同时在多个节点上运行
    * HTTP任务
    > 访问指定的URL地址, 由调度器直接执行, 不依赖任务节点
    * 证书续期
    > 自动根据证书的相关信息（域名信息、阿里云Accesskey、Email）自动续期并更换slb上的证书
* 查看任务执行结果日志
* 任务执行结果通知, 支持邮件、Slack、Webhook

### 截图
![流程图](https://raw.githubusercontent.com/ouqiang/gocron/master/assets/screenshot/scheduler.png)
![任务](https://raw.githubusercontent.com/ouqiang/gocron/master/assets/screenshot/task.png)
![Slack](https://raw.githubusercontent.com/ouqiang/gocron/master/assets/screenshot/notification.png)
    
### 支持平台
> Windows、Linux、Mac OS

### 环境要求
>  MySQL


## 安装

###  二进制安装
1. 解压压缩包   
2. `cd 解压目录`   
3. 启动        
* 调度器启动        
  * Windows: `gocron.exe web`   
  * Linux、Mac OS:  `./gocron web`
* 任务节点启动, 默认监听0.0.0.0:5921
  * Windows:  `gocron-node.exe`
  * Linux、Mac OS:  `./gocron-node`
4. 浏览器访问 http://localhost:5920


## To Do List
- [ ] 支持节点部署或更新ssl证书；
- [ ] 域名申请支持http-01，tls-01认证；
- [ ] 考虑更换xorm为gorm；

## 程序使用的组件
* 定时任务管理系统 [cocron](https://github.com/ouqiang/gocron)
* Acme的golang实现 [lego](https://github.com/go-acme/lego)
* Web框架 [Macaron](http://go-macaron.com/)
* 定时任务调度 [Cron](https://github.com/robfig/cron)
* ORM [Xorm](https://github.com/go-xorm/xorm)
* UI框架 [Element UI](https://github.com/ElemeFE/element)
* 依赖管理 [Govendor](https://github.com/kardianos/govendor)
* RPC框架 [gRPC](https://github.com/grpc/grpc)

## 反馈
提交[issue](https://github.com/ouqiang/gocron/issues/new)
