package setting

import (
	"errors"
	"os"

	"gitee.com/enlangs/goautocert/internal/modules/logger"
	"gitee.com/enlangs/goautocert/internal/modules/utils"
	"gopkg.in/ini.v1"
)

const DefaultSection = "default"

type Setting struct {
	Db struct {
		Engine       string
		Host         string
		Port         string
		User         string
		Password     string
		Database     string
		Prefix       string
		Charset      string
		MaxIdleConns int
		MaxOpenConns int
	}
	AllowIps      string
	AppName       string
	ApiKey        string
	ApiSecret     string
	ApiSignEnable bool

	EnableTLS bool
	CAFile    string
	CertFile  string
	KeyFile   string

	ConcurrencyQueue int
	AuthSecret       string
}

// 读取配置
func Read(filename string) (*Setting, error) {
	// config, err := ini.Load(filename)
	// if err != nil {
	// 	return nil, err
	// }
	// section := config.Section(DefaultSection)

	var s Setting

	s.Db.Engine = os.Getenv("DB_ENGINE")     // section.Key("db.engine").MustString("mysql")
	s.Db.Host = os.Getenv("DB_HOST")         // section.Key("db.host").MustString("127.0.0.1")
	s.Db.Port = os.Getenv("DB_PORT")         // section.Key("db.port").MustInt(3306)
	s.Db.User = os.Getenv("DB_USER")         // section.Key("db.user").MustString("")
	s.Db.Password = os.Getenv("DB_PASSWORD") // section.Key("db.password").MustString("")
	s.Db.Database = os.Getenv("DB_DATABASE") // section.Key("db.database").MustString("autocert")
	s.Db.Prefix = os.Getenv("DB_PREFIX")     // section.Key("db.prefix").MustString("")
	s.Db.Charset = os.Getenv("DB_CHARSET")   // section.Key("db.charset").MustString("utf8")
	s.Db.MaxIdleConns = 30                   // section.Key("db.max.idle.conns").MustInt(30)
	s.Db.MaxOpenConns = 100                  // section.Key("db.max.open.conns").MustInt(100)

	s.AllowIps = os.Getenv("ALLOW_IPS")     // section.Key("allow_ips").MustString("")
	s.AppName = "定时任务管理系统"                  // section.Key("app.name").MustString("定时任务管理系统")
	s.ApiKey = os.Getenv("API_KEY")         // section.Key("api.key").MustString("")
	s.ApiSecret = os.Getenv("API_SECRET")   // section.Key("api.secret").MustString("")
	s.ApiSignEnable = true                  // section.Key("api.sign.enable").MustBool(true)
	s.ConcurrencyQueue = 500                // section.Key("concurrency.queue").MustInt(500)
	s.AuthSecret = os.Getenv("AUTH_SECRET") // section.Key("auth_secret").MustString("")
	if s.AuthSecret == "" {
		s.AuthSecret = utils.RandAuthToken()
	}

	s.EnableTLS = false // section.Key("enable_tls").MustBool(false)
	s.CAFile = ""       // section.Key("ca_file").MustString("")
	s.CertFile = ""     // section.Key("cert_file").MustString("")
	s.KeyFile = ""      // section.Key("key_file").MustString("")

	if s.EnableTLS {
		if !utils.FileExist(s.CAFile) {
			logger.Fatalf("failed to read ca cert file: %s", s.CAFile)
		}

		if !utils.FileExist(s.CertFile) {
			logger.Fatalf("failed to read client cert file: %s", s.CertFile)
		}

		if !utils.FileExist(s.KeyFile) {
			logger.Fatalf("failed to read client key file: %s", s.KeyFile)
		}
	}

	return &s, nil
}

// 写入配置
func Write(config []string, filename string) error {
	if len(config) == 0 {
		return errors.New("参数不能为空")
	}
	if len(config)%2 != 0 {
		return errors.New("参数不匹配")
	}

	file := ini.Empty()

	section, err := file.NewSection(DefaultSection)
	if err != nil {
		return err
	}
	for i := 0; i < len(config); {
		_, err = section.NewKey(config[i], config[i+1])
		if err != nil {
			return err
		}
		i += 2
	}
	err = file.SaveTo(filename)

	return err
}
