package utils

import (
	"github.com/go-xorm/xorm"
)

type InTransaction func(tx *xorm.Session) error

// 在事务中执行数据库操作,且统一事务rollback和commit.  避免出现事务没有commit或rollback的情况！
func DoInTransaction(db *xorm.Engine, fn InTransaction) error {
	session := db.NewSession()
	if err := session.Begin(); err != nil {
		return err
	}
	err := fn(session)
	if err != nil {

		if xerr := session.Rollback(); xerr != nil {
			return xerr
		}
		return err
	}
	if err = session.Commit(); err != nil {
		return err
	}
	return nil
}
