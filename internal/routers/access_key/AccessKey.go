package access_key

import (
	"gitee.com/enlangs/goautocert/internal/models"
	"gitee.com/enlangs/goautocert/internal/modules/logger"
	"gitee.com/enlangs/goautocert/internal/modules/utils"
	"gitee.com/enlangs/goautocert/internal/routers/base"
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
	"strconv"
	"strings"
)

// Index AccessKey列表
func Index(ctx *macaron.Context) string {
	accessKey := new(models.AccessKey)
	queryParams := parseQueryParams(ctx)
	total, err := accessKey.Total(queryParams)
	if err != nil {
		logger.Error(err)
	}
	hosts, err := accessKey.List(queryParams)
	if err != nil {
		logger.Error(err)
	}

	jsonResp := utils.JsonResponse{}

	return jsonResp.Success(utils.SuccessContent, map[string]interface{}{
		"total": total,
		"data":  hosts,
	})
}

// All 获取所有 AccessKey
func All(ctx *macaron.Context) string {
	accessKey := new(models.AccessKey)
	accessKey.PageSize = -1
	hosts, err := accessKey.List(models.CommonMap{})
	if err != nil {
		logger.Error(err)
	}

	jsonResp := utils.JsonResponse{}

	return jsonResp.Success(utils.SuccessContent, hosts)
}

// Detail AccessKey详情
func Detail(ctx *macaron.Context) string {
	accessKey := new(models.AccessKey)
	id := ctx.ParamsInt(":id")
	err := accessKey.Find(id)
	jsonResp := utils.JsonResponse{}
	if err != nil || accessKey.Id == 0 {
		logger.Errorf("获取AccessKey详情失败#AccessKeyid-%d", id)
		return jsonResp.Success(utils.SuccessContent, nil)
	}

	return jsonResp.Success(utils.SuccessContent, accessKey)
}

type AccessKeyForm struct {
	Id     int
	Key    string `binding:"Required;MaxSize(64)"`
	Secret string `binding:"Required;MaxSize(128)"`
	Remark string
}

// Error 表单验证错误处理
func (f AccessKeyForm) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) == 0 {
		return
	}
	json := utils.JsonResponse{}
	content := json.CommonFailure("表单验证失败, 请检测输入")
	_, _ = ctx.Write([]byte(content))
}

// Store 保存、修改AccessKey信息
func Store(ctx *macaron.Context, form AccessKeyForm) string {
	json := utils.JsonResponse{}
	accessKey := new(models.AccessKey)
	id := form.Id
	nameExist, err := accessKey.KeyAndSecretExists(form.Key, form.Secret)
	if err != nil {
		return json.CommonFailure("操作失败", err)
	}
	if nameExist {
		return json.CommonFailure("AccessKey名已存在")
	}

	accessKey.AccessKeyId = strings.TrimSpace(form.Key)
	accessKey.AccessKeySecret = strings.TrimSpace(form.Secret)
	accessKey.Remark = strings.TrimSpace(form.Remark)
	oldAccessKeyModel := new(models.AccessKey)
	err = oldAccessKeyModel.Find(int(id))
	if err != nil {
		return json.CommonFailure("AccessKey不存在")
	}

	if id > 0 {
		_, err = accessKey.UpdateBean(id)
	} else {
		id, err = accessKey.Create()
	}
	if err != nil {
		return json.CommonFailure("保存失败", err)
	}

	return json.Success("保存成功", nil)
}

// Remove 删除AccessKey
func Remove(ctx *macaron.Context) string {
	id, err := strconv.Atoi(ctx.Params(":id"))
	json := utils.JsonResponse{}
	if err != nil {
		return json.CommonFailure("参数错误", err)
	}

	accessKey := new(models.AccessKey)
	err = accessKey.Find(int(id))
	if err != nil {
		return json.CommonFailure("AccessKey不存在")
	}

	_, err = accessKey.Delete(id)
	if err != nil {
		return json.CommonFailure("操作失败", err)
	}

	return json.Success("操作成功", nil)
}

// 解析查询参数
func parseQueryParams(ctx *macaron.Context) models.CommonMap {
	var params = models.CommonMap{}
	params["Id"] = ctx.QueryInt("id")
	base.ParsePageAndPageSize(ctx, params)

	return params
}
