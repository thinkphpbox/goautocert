package certificate

import (
	"gitee.com/enlangs/goautocert/internal/models"
	"gitee.com/enlangs/goautocert/internal/modules/letsencrypt"
	"gitee.com/enlangs/goautocert/internal/modules/utils"
	"gopkg.in/macaron.v1"
)

// 申请证书
func Obtain(ctx *macaron.Context) string {

	jsonResp := utils.JsonResponse{}
	domainConfigId := ctx.QueryInt("domainConfigId")
	acmeUserId := ctx.QueryInt("acmeUserId")
	accessKeyId := ctx.QueryInt("accessKeyId")

	var au models.AcmeUser
	if err := au.Find(acmeUserId); err != nil {
		return jsonResp.Failure(100, "获取Acme账号配置信息失败，详情："+err.Error())
	}
	var ak models.AccessKey
	if err := ak.Find(accessKeyId); err != nil {
		return jsonResp.Failure(110, "获取AccessKey配置信息失败，详情："+err.Error())
	}
	var v models.DomainConfig
	if err := v.Find(domainConfigId); err != nil {
		return jsonResp.Failure(120, "获取域名配置失败，详情："+err.Error())
	}

	if result, err := letsencrypt.ObtainCertificate(&au, v, ak); err != nil {
		return jsonResp.Failure(130, "申请证书失败，详情："+err.Error())
	} else if certId, err := result.Create(); err != nil {
		return jsonResp.Failure(132, "证书存储失败，详情："+err.Error())
	} else {
		return jsonResp.Success(utils.SuccessContent, map[string]interface{}{
			"certId":         certId,
			"domainConfigId": domainConfigId,
			"acmeUserId":     acmeUserId,
			"accessKeyId":    accessKeyId,
		})
	}
}

// 证书续期
func Renew(ctx *macaron.Context) string {

	jsonResp := utils.JsonResponse{}
	domainConfigId := ctx.QueryInt("domainConfigId")
	acmeUserId := ctx.QueryInt("acmeUserId")
	accessKeyId := ctx.QueryInt("accessKeyId")
	certId := ctx.QueryInt("certId")

	var au models.AcmeUser
	if err := au.Find(acmeUserId); err != nil {
		return jsonResp.Failure(100, "获取Acme账号配置信息失败，详情："+err.Error())
	}
	var ak models.AccessKey
	if err := ak.Find(accessKeyId); err != nil {
		return jsonResp.Failure(110, "获取AccessKey配置信息失败，详情："+err.Error())
	}
	var v models.DomainConfig
	if err := v.Find(domainConfigId); err != nil {
		return jsonResp.Failure(120, "获取域名配置失败，详情："+err.Error())
	}

	var cert models.Certificate
	if err := cert.Find(certId); err != nil {
		return jsonResp.Failure(120, "获取证书内容失败，详情："+err.Error())
	}

	if result, err := letsencrypt.RenewCertificate(&au, v, ak, cert); err != nil {
		return jsonResp.Failure(130, "证书续期操作失败，详情："+err.Error())
	} else if certId, err := result.Create(); err != nil {
		return jsonResp.Failure(132, "证书存储失败，详情："+err.Error())
	} else {
		return jsonResp.Success(utils.SuccessContent, map[string]interface{}{
			"certId":         certId,
			"domainConfigId": domainConfigId,
			"acmeUserId":     acmeUserId,
			"accessKeyId":    accessKeyId,
		})
	}
}

// 证书注销
func Revoke(ctx *macaron.Context) string {
	jsonResp := utils.JsonResponse{}
	acmeUserId := ctx.QueryInt("acmeUserId")
	certId := ctx.QueryInt("certId")

	var au models.AcmeUser
	if err := au.Find(acmeUserId); err != nil {
		return jsonResp.Failure(100, "获取Acme账号配置信息失败，详情："+err.Error())
	}

	var cert models.Certificate
	if err := cert.Find(certId); err != nil {
		return jsonResp.Failure(120, "获取证书内容失败，详情："+err.Error())
	}

	if result, err := letsencrypt.RevokeCertificate(&au, cert); err != nil {
		return jsonResp.Failure(130, "证书续期操作失败，详情："+err.Error())
	} else if certId, err := result.Create(); err != nil {
		return jsonResp.Failure(132, "证书存储失败，详情："+err.Error())
	} else {
		return jsonResp.Success(utils.SuccessContent, map[string]interface{}{
			"certId":     certId,
			"acmeUserId": acmeUserId,
		})
	}
}
