package acme_user

import (
	"gitee.com/enlangs/goautocert/internal/models"
	"gitee.com/enlangs/goautocert/internal/modules/logger"
	"gitee.com/enlangs/goautocert/internal/modules/utils"
	"gitee.com/enlangs/goautocert/internal/routers/base"
	"github.com/go-macaron/binding"
	"gopkg.in/macaron.v1"
	"strconv"
	"strings"
)

// Index AcmeUser列表
func Index(ctx *macaron.Context) string {
	acmeUser := new(models.AcmeUser)
	queryParams := parseQueryParams(ctx)
	total, err := acmeUser.Total(queryParams)
	if err != nil {
		logger.Error(err)
	}
	hosts, err := acmeUser.List(queryParams)
	if err != nil {
		logger.Error(err)
	}

	jsonResp := utils.JsonResponse{}

	return jsonResp.Success(utils.SuccessContent, map[string]interface{}{
		"total": total,
		"data":  hosts,
	})
}

// All 获取所有 AcmeUser
func All(ctx *macaron.Context) string {
	acmeUser := new(models.AcmeUser)
	acmeUser.PageSize = -1
	acmeUsers, err := acmeUser.List(models.CommonMap{})
	if err != nil {
		logger.Error(err)
	}

	jsonResp := utils.JsonResponse{}

	return jsonResp.Success(utils.SuccessContent, acmeUsers)
}

// Detail AcmeUser详情
func Detail(ctx *macaron.Context) string {
	acmeUser := new(models.AcmeUser)
	id := ctx.ParamsInt(":id")
	err := acmeUser.Find(id)
	jsonResp := utils.JsonResponse{}
	if err != nil || acmeUser.Id == 0 {
		logger.Errorf("获取AcmeUser详情失败#AcmeUserid-%d", id)
		return jsonResp.Success(utils.SuccessContent, nil)
	}

	return jsonResp.Success(utils.SuccessContent, acmeUser)
}

type AcmeUserForm struct {
	Id     int
	Email  string `binding:"Required;MaxSize(64)"`
	Remark string
}

// Error 表单验证错误处理
func (f AcmeUserForm) Error(ctx *macaron.Context, errs binding.Errors) {
	if len(errs) == 0 {
		return
	}
	json := utils.JsonResponse{}
	content := json.CommonFailure("表单验证失败, 请检测输入")
	_, _ = ctx.Write([]byte(content))
}

// Store 保存、修改AcmeUser信息
func Store(ctx *macaron.Context, form AcmeUserForm) string {
	json := utils.JsonResponse{}
	acmeUser := new(models.AcmeUser)
	id := form.Id
	nameExist, err := acmeUser.EmailExists(form.Email, form.Id)
	if err != nil {
		return json.CommonFailure("操作失败", err)
	}
	if nameExist {
		return json.CommonFailure("AcmeUser名已存在")
	}

	acmeUser.Email = strings.TrimSpace(form.Email)
	acmeUser.Remark = strings.TrimSpace(form.Remark)
	oldAcmeUserModel := new(models.AcmeUser)
	err = oldAcmeUserModel.Find(int(id))
	if err != nil {
		return json.CommonFailure("AcmeUser不存在")
	}

	if id > 0 {
		_, err = acmeUser.UpdateBean(id)
	} else {
		id, err = acmeUser.Create()
	}
	if err != nil {
		return json.CommonFailure("保存失败", err)
	}

	return json.Success("保存成功", nil)
}

// Remove 删除AcmeUser
func Remove(ctx *macaron.Context) string {
	id, err := strconv.Atoi(ctx.Params(":id"))
	json := utils.JsonResponse{}
	if err != nil {
		return json.CommonFailure("参数错误", err)
	}

	acmeUser := new(models.AcmeUser)
	err = acmeUser.Find(int(id))
	if err != nil {
		return json.CommonFailure("AcmeUser不存在")
	}

	_, err = acmeUser.Delete(id)
	if err != nil {
		return json.CommonFailure("操作失败", err)
	}

	return json.Success("操作成功", nil)
}

// 解析查询参数
func parseQueryParams(ctx *macaron.Context) models.CommonMap {
	var params = models.CommonMap{}
	params["Id"] = ctx.QueryInt("id")
	params["Email"] = ctx.QueryTrim("email")
	base.ParsePageAndPageSize(ctx, params)

	return params
}
