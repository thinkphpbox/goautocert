// Command autocert-node
package main

import (
	"flag"
	"os"
	"runtime"
	"strings"

	"gitee.com/enlangs/goautocert/internal/modules/rpc/auth"
	"gitee.com/enlangs/goautocert/internal/modules/rpc/server"
	"gitee.com/enlangs/goautocert/internal/modules/utils"
	"github.com/ouqiang/goutil"
	log "github.com/sirupsen/logrus"
)

var (
	AppVersion, BuildDate, GitCommit string
)

func main() {
	var serverAddr string
	var allowRoot bool
	var version bool
	var CAFile string
	var certFile string
	var keyFile string
	var enableTLS bool
	var logLevel string
	flag.BoolVar(&allowRoot, "allow-root", false, "./autocert-node -allow-root")
	flag.StringVar(&serverAddr, "s", "0.0.0.0:5921", "./autocert-node -s ip:port")
	flag.BoolVar(&version, "v", false, "./autocert-node -v")
	flag.BoolVar(&enableTLS, "enable-tls", false, "./autocert-node -enable-tls")
	flag.StringVar(&CAFile, "ca-file", "", "./autocert-node -ca-file path")
	flag.StringVar(&certFile, "cert-file", "", "./autocert-node -cert-file path")
	flag.StringVar(&keyFile, "key-file", "", "./autocert-node -key-file path")
	flag.StringVar(&logLevel, "log-level", "info", "-log-level error")
	flag.Parse()
	level, err := log.ParseLevel(logLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(level)

	if version {
		goutil.PrintAppVersion(AppVersion, GitCommit, BuildDate)
		return
	}

	if enableTLS {
		if !utils.FileExist(CAFile) {
			log.Fatalf("failed to read ca cert file: %s", CAFile)
		}
		if !utils.FileExist(certFile) {
			log.Fatalf("failed to read server cert file: %s", certFile)
			return
		}
		if !utils.FileExist(keyFile) {
			log.Fatalf("failed to read server key file: %s", keyFile)
			return
		}
	}

	certificate := auth.Certificate{
		CAFile:   strings.TrimSpace(CAFile),
		CertFile: strings.TrimSpace(certFile),
		KeyFile:  strings.TrimSpace(keyFile),
	}

	if runtime.GOOS != "windows" && os.Getuid() == 0 && !allowRoot {
		log.Fatal("Do not run autocert-node as root user")
		return
	}

	server.Start(serverAddr, enableTLS, certificate)
}
